// Incapsulation.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>
#include <math.h>
using namespace std;

class Vector {

private:
    float x;
    float y;
    float z;

public:

    Vector(float x, float y, float z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    float getX() {
        return x;
    }

    float getY() {
        return y;
    }

    float getZ() {
        return z;
    }

    float getVectorModule() {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }
};

int main()
{
    Vector vectorTest(7, 8, 9.23);
    cout << "x = " << vectorTest.getX() << " y = " << vectorTest.getY() << " z = " << vectorTest.getZ() << "\n";
    cout << vectorTest.getVectorModule();
}
